#include "Functions.h"
#include "SmartArray.h"

int main()
{
	int quantity = 100;
	float price = 9.99;
	std::string name = "Pizza";

	DisplayString("name", name);
	DisplayInt("quantity", quantity);
	DisplayFloat("price", price);

    SmartArray<std::string> productNames;

    productNames.Insert(0, "Notebook");
    productNames.Insert(1, "Pencils");
    productNames.Insert(2, "Eraser");
    productNames.Display();

	return 0;
}