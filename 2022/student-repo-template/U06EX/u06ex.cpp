#include <iostream>
using namespace std;

void Program1();
void Program2();
void Program3();

int main()
{
    cout << "Run which program? (1-3): ";
    int prog;
    cin >> prog;

    switch( prog )
    {
        case 1: Program1(); break;
        case 2: Program2(); break;
        case 3: Program3(); break;
    }

    return 0;
}
