#include "Functions.h"
#include <stdexcept>
#include <iostream>

float Divide( float num, float denom )
{
     if (denom == 0)
    {
        throw std::invalid_argument("Division by 0 not allowed!");
    }
    return num / denom;
}

void Display(std::array<std::string, 5> arr, int index)
{
    if (index < 0 || index >= arr.size())
    {
        throw std::out_of_range("Index out of bounds!");
    }
    std::cout << "Item at index " << index << " is " << arr[index] << std::endl;
}

void PtrDisplay(int* arr, int size, int index)
{
    if (arr == nullptr)
    {
        throw std::invalid_argument("ptr is null!");
    }
    if (index < 0 || index >= size)
    {
        throw std::out_of_range("Not in range!");
    }
    std::cout << "Item at index " << index << " is " << arr[index] << std::endl;
}

int SlicesPerPerson(int friends, int pizzaSlices)
{
     if (friends == 0)
    {
        throw NotEnoughFriendsException("Exception: Zero friends at party!");
    }
    if (pizzaSlices == 0)
    {
        throw NotEnoughPizzaException("Exception: Zero pizza at party!");
    }
    return float(pizzaSlices) / friends;
}
