#include <iostream>
#include <cstdlib>
using namespace std;

#include "Functions.h"

void Program1()
{
	cout << endl << "------------------------------------------" << endl;
	cout << "DIVISION EXAMPLE" << endl;

	float numerator, denominator;
	cout << "Enter a numerator: ";
	cin >> numerator;

	cout << "Enter a denominator: ";
	cin >> denominator;

	float quotient;

	cout << endl;

    try
	{
		quotient = Divide(numerator, denominator);
		cout << "Quotient: " << quotient << endl;
	}
	catch (const std::invalid_argument& ex)
	{
		cout << "invalid_argument Exception: " << ex.what() << endl;
	}
}

void Program2()
{
	cout << endl << "------------------------------------------" << endl;
	cout << "ARRAY EXAMPLE" << endl;

	array<string, 5> myArray = { "cat", "bat", "rat", "gnat", "goat" };

	cout << "Display item at which index? ";
	int index;
	cin >> index;

	cout << endl;

    try
	{
		Display(myArray, index);
	}
	catch (const std::out_of_range& ex)
	{
		cout << "out_of_range Exception: " << ex.what() << endl;
	}

void Program3()
{
	cout << endl << "------------------------------------------" << endl;
	cout << "MULTIPLE EXCEPTIONS EXAMPLE" << endl;

	int* dynamicArray = new int[4];
	dynamicArray[0] = rand() % 100;
	dynamicArray[1] = rand() % 100;
	dynamicArray[2] = rand() % 100;
	dynamicArray[3] = rand() % 100;

	int* badPointer = nullptr;

	cout << "Display item at which index? ";
	int index;
	cin >> index;

	cout << endl;

    try
	{
		cout << "dynamicArray ";
		PtrDisplay(dynamicArray, 4, index);

		cout << endl << "badPointer ";
		PtrDisplay(badPointer, 4, index);
	}
	catch (const std::invalid_argument& ex)
	{
		cout << "invalid_argument Exception: " << ex.what() << endl;
	}
	catch (const std::out_of_range& ex)
	{
		cout << "out_of_range Exception: " << ex.what() << endl;
	}
	delete[] dynamicArray;
}

void Program4()
{
	cout << endl << "------------------------------------------" << endl;
	cout << "CUSTOM EXAMPLE" << endl;

	int friendCount, pizzaSliceCount;
	cout << "How many pizza slices at pizza party? ";
	cin >> pizzaSliceCount;

	cout << "How many friends at party? ";
	cin >> friendCount;

	cout << endl;

    try
	{
		int slices = SlicesPerPerson(friendCount, pizzaSliceCount);
		cout << "Give each friend " << slices << " slices of pizza" << endl;
	}
	catch (const NotEnoughFriendsException& ex)
	{
		cout << "You should really make more friends." << endl;
	}
	catch (const NotEnoughPizzaException& ex)
	{
		cout << "You can't have a pizza party without pizza!" << endl;
	}
}

int main()
{

	bool done = false;
	while (!done)
	{
		cout << "----------------------------------------" << endl;
		cout << "- MAIN MENU                            -" << endl;
		cout << "----------------------------------------" << endl;
		cout << "- 0. EXIT                              -" << endl;
		cout << "- 1. Division example                  -" << endl;
		cout << "- 2. Array example                     -" << endl;
		cout << "- 3. Multiple exceptions example       -" << endl;
		cout << "- 4. Custom example                    -" << endl;
		cout << "----------------------------------------" << endl;
		cout << endl << "SELECTION: ";
		int choice;
		cin >> choice;

		cout << endl << endl;
		cout << "-------------[SELECTED " << choice << "]-------------" << endl;

		switch (choice)
		{
		case 0:     done = true;        break;
		case 1:     Program1();         break;
		case 2:     Program2();         break;
		case 3:     Program3();         break;
		case 4:     Program4();         break;
		}

		cout << endl << endl;
	}

	return 0;
}
