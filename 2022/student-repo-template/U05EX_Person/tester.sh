clear
echo "-------------------------------------------------"
echo "Build program..."
g++ *.h *.cpp -o u05ex_exe
echo "-------------------------------------------------"
echo "TEST: Run program"
echo ""
echo "EXPECTED OUTPUT:"
echo "1. User is asked to enter new Customer name and tier."
echo "2. User is asked to enter new Employee name and wage."
echo "3. Table of Customers and Employees is displayed."
echo "4. Memory must be freed at the end."
echo ""
echo "ACTUAL OUTPUT:"
echo Bob 1 Bobbert 12.34 | ./u05ex_exe
echo ""
echo "-------------------------------------------------"
echo "PROGRAM CODE:"
cat *.cpp
cat *.h
