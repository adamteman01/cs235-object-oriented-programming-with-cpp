#include <iostream>
using namespace std;

#include "FunctionsTests.h"

int main()
{
    Test_IsOverdrawn();
    Test_AdjustIngredients();
    Test_IsInputValid();
    Test_Average();

    return 0;
}
