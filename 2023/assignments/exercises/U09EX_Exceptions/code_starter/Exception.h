#ifndef _EXCEPTION
#define _EXCEPTION

#include <stdexcept>
#include <iostream>

class NotEnoughFriendsException : public std::runtime_error
{
public:
	NotEnoughFriendsException(std::string message)
		: std::runtime_error(message) {
		std::cout << "Exception: Zero friends at party!" << std::endl;
	}
	
};
class NotEnoughPizzaException : public std::runtime_error
{
public:
	NotEnoughPizzaException(std::string message)
		: std::runtime_error(message) {
		std::cout << "Exception: Zero pizza at party!" << std::endl;
	}
};

#endif

