#include "p3_MyString.h"

#include <stdexcept>
#include <iostream>

// Constructors
MyString::MyString()
{
    // Don't need to do anything
}

MyString::MyString( string text )
{
}


// Assignment operator
MyString& MyString::operator=( const MyString& other )
{
    return *this; // Placeholder
}


// Subscript operator
char& MyString::operator[] ( const int index )
{
    return m_text[0]; // Placeholder - remove me
}


// Stream operators
ostream& operator<<( ostream& out, MyString& item )
{
    return out; // Placeholder
}

istream& operator>>( istream& in, MyString& item )
{
    return in; // Placeholder
}


// Arithmetic operators
MyString operator+( const MyString& item1, const MyString& item2 )
{
    return MyString(); // Placeholder - remove me
}

MyString operator+( const MyString& item1, char letter )
{
    return MyString(); // Placeholder - remove me
}


// Comparison operators
bool operator==( const MyString& item1, const MyString& item2 )
{
    return false; // Placeholder - remove me
}

bool operator!=( const MyString& item1, const MyString& item2 )
{
    return false; // Placeholder - remove me
}

