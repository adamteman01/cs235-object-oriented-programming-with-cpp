#ifndef _MYSTRING_H
#define _MYSTRING_H

#include <vector>
#include <string>
using namespace std;

class MyString
{
    public:
    // Constructors
    MyString();
    MyString( string text );

    // Assignment operator
    MyString& operator=( const MyString& other );

    // Subscript operator
    char& operator[] ( const int index );

    // Stream operators
    friend ostream& operator<<( ostream& out, MyString& item );
    friend istream& operator>>( istream& in, MyString& item );

    // Arithmetic operators
    friend MyString operator+( const MyString& item1, const MyString& item2 );
    friend MyString operator+( const MyString& item1, char letter );

    // Comparison operators
    friend bool operator==( const MyString& item1, const MyString& item2 );
    friend bool operator!=( const MyString& item1, const MyString& item2 );

    private:
    vector<char> m_text;
};

#endif
