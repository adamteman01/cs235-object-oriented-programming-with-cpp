#include <iostream>
#include <string>
using namespace std;

struct Node
{
    Node()
    {
        ptrNext = nullptr;
    }

    Node( string value )
    {
        data = value;
        ptrNext = nullptr;
    }

    Node* ptrNext;
    string data;
};

void Program3()
{
    cout << endl << "LINKED NODES" << endl;

    /*
    ABOUT DYNAMIC VARIABLES:
    We can dynamically allocate space for a single variable on HEAP MEMORY by using pointers.
    This is usually useful for writing certain DATA STRUCTURES, liked Linked Lists and Trees.

    Remember that when we manually ALLOCATE space for a variable (using the "new" keyword)
    we need to also manually DEALLOCATE space before the pointer loses scope (using the "delete" keyword).
    */

    cout << endl << "DYNAMICALLY ALLOCATING MEMORY FOR VARIABLES" << endl << string( 80, '-' ) << endl;

    cout << "CREATE NODES AND LINK THEM:" << endl;
    cout << "* Create firstNode (A)" << endl;
    // Allocate space for one Node object via a pointer. Set its data value to "A".

    cout << "* Create firstNode->ptrNext (B)" << endl;
    // Allocate space for another Node object via the first node's ->ptrNext pointer. Set its data value to "B".

    cout << "* Create firstNode->ptrNext->ptrNext (C)" << endl;
    // Allocate space for another Node object via the first node's ->ptrNext->ptrNext pointer. Set its data value to "C.

    cout << endl << "ITERATE THROUGH NODES:" << endl;
    // Create a pointer named ptrCurrent to point to the existing firstNode.
    // While ptrCurrent is not equal to nullptr, display the data of the current item (ptrCurrent->data),
    // and then traverse forwards (ptrCurrent = ptrCurrent->ptrNext).

    cout << endl << "FREE MEMORY:" << endl;
    cout << "* Delete firstNode->ptrNext->ptrNext" << endl;
    // Free memory of the node at firstNode->ptrNext->ptrNext and afterward set it to point to nullptr

    cout << "* Delete firstNode->ptrNext" << endl;
    // Free memory of the node at firstNode->ptrNext and afterward set it to point to nullptr

    cout << "* Delete firstNode" << endl;
    // Free memory of the node at firstNode and afterward set it to point to nullptr

    cout << endl << "Press ENTER to continue." << endl;
    string a;
    cin.ignore();
    getline( cin, a );

    cout << endl << "THE END" << endl;
}
