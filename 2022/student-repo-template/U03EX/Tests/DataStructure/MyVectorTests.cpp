#include "MyVectorTests.h"

#include <iostream>
using namespace std;

void MyVectorTester::Test_Constructor()
{
    cout << endl << string( 80, '-' ) << endl << "Test_Constructor" << endl;

}

void MyVectorTester::Test_AllocateMemory()
{
    cout << endl << string( 80, '-' ) << endl << "Test_AllocateMemory" << endl;

}

void MyVectorTester::Test_DeallocateMemory()
{
    cout << endl << string( 80, '-' ) << endl << "Test_DeallocateMemory" << endl;

}

void MyVectorTester::Test_PushBack()
{
    cout << endl << string( 80, '-' ) << endl << "Test_PushBack" << endl;

}

void MyVectorTester::Test_PopAt()
{
    cout << endl << string( 80, '-' ) << endl << "Test_PopAt" << endl;

}

void MyVectorTester::Test_GetAt()
{
    cout << endl << string( 80, '-' ) << endl << "Test_GetAt" << endl;

}

void MyVectorTester::Test_Size()
{
    cout << endl << string( 80, '-' ) << endl << "Test_Size" << endl;

}

void MyVectorTester::Test_Resize()
{
    cout << endl << string( 80, '-' ) << endl << "Test_Resize" << endl;

}

void MyVectorTester::Test_IsFull()
{
    cout << endl << string( 80, '-' ) << endl << "Test_IsFull" << endl;

}
