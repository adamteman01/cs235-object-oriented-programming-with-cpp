#include "Functions.h"
#include <iostream>
using namespace std;

void DisplayInt(string label, int value)
{
    cout << label << ": " << value << endl;
}

void DisplayString(string label, string value)
{
    cout << label << ": " << value << endl;
}

void DisplayFloat(string label, float value)
{
    cout << label << ": " << value << endl;
}