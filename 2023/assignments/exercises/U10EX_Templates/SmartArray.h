#ifndef _SMARTARRAY_H
#define _SMARTARRAY_H
#include <string>
#include <iostream>
using namespace std;


template <typename T>
void Display(string label, T value)
{
    std::cout << label << ": " << value << std::endl;
}
template <typename T>
class SmartArray
{
public:
    SmartArray();
    size_t GetSize();
    T& GetAt(size_t index);
    void Insert(size_t index, T value);
    void Display();
private:
    T m_array[3];
    size_t m_arraySize;
};
template <typename T>
SmartArray<T>::SmartArray()
{
    m_arraySize = 3;
}

template <typename T>
size_t SmartArray<T>::GetSize()
{
    return m_arraySize, size_t;
}
template <typename T>
T& SmartArray<T>::GetAt(size_t index)
{
    if (index < 0 || index > m_arraySize)
    {
        throw out_of_range("Invalid index!");
    }
    return T& m_array[index];
}
template <typename T>
void SmartArray<T>::Insert(size_t index, T value)
{
    if (index < 0 || index > m_arraySize)
    {
        throw out_of_range("Invalid index!");
    }
    m_array[index] = value;
}
template <typename T>
void SmartArray<T>::Display()
{
    for (int i = 0; i < m_arraySize; ++i)
    {
        cout << i << ": " << (m_array[i]) << endl;
    }
}

#endif
