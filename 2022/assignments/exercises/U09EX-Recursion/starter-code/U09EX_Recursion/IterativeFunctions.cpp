#include "IterativeFunctions.h"
#include <iostream> /* Use cout */
using namespace std;

// SET 1: Steps are given ------------------------------------------------------------

void CountUp_Iter(int start, int end) {
  // 1. Create a FOR LOOP
  // ** Starting code: Create a counter `i`, initialize to `start`
  // ** Condition: Continue looping while `i` is less than or equal to `end`
  // ** Update: Add 1 to `i` each iteration
  // Within the for loop:
  //  1a. Display the value of `i` and a space
  // The end
}

int SumUp_Iter(int start, int end) {
  // 1. Create an integer variable named `sum`, initialize it to `0`.
  // 2. Create a FOR LOOP
  // ** Starting code: Create a counter `i`, initialize to `start`
  // ** Condition: Continue looping while `i` is less than or equal to `end`
  // ** Update: Add 1 to `i` each iteration
  // Within the for loop:
  //  2a. Add the value of `i` onto the value of `sum`. Make sure the `sum` variable is overwritten.
  // After the loop:
  // 3. Return `sum`.

    return 1; // TEMPORARY - REMOVE ONCE YOU IMPLEMENT THIS FUNCTION.
}

void Display_Iter(vector<string> list) {
  // 1. Create a for loop:
  // ** STARTER: Create an integer `i` and initialize it to `0`.
  // ** CONDITION: Continue looping while `i` is less than the `list.size()`.
  // ** UPDATE: Add 1 to `i` each iteration.
  // Within the loop:
  //  1a. Display the index and value of the item at that index
}

// SET 2: Steps aren't given ------------------------------------------------------------

void DisplayChars_Iter(string str)
{
    // Iterate over the string with a for loop (you can use str.size() to get its size)
    // and display the index of each character and each character.
}

int Factorial_Iter( int n )
{
    // Use a for loop to calculate the product of numbers from
    // n to 1. For example, n! = n (n-1) (n-2) ... (2) (1).

    return 1; // TEMPORARY - REMOVE ONCE YOU IMPLEMENT THIS FUNCTION.
}
