cat main.cpp

echo "---- RESULT FILES? ----"
echo "Filename 1:"
read file1
echo "Filename 2:"
read file2

echo ""
echo "---- BUILD PROGRAM ----"
g++ *.cpp *.h Utilities/Helper.cpp Utilities/Helper.hpp -o ImageProgram

echo ""
echo "---- RUN PROGRAM ----"
./ImageProgram > run

echo ""
echo "---- OPEN IMAGES IN GIMP ----"
gimp images/draw.ppm
gimp images/$file1
gimp images/bunny2.ppm
gimp images/$file2

echo ""
echo "---- SOURCE FILES ---- "
geany Image.cpp
geany main.cpp
